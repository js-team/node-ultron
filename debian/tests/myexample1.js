'use strict';
 
var Ultron = require('ultron');
var EventEmitter = require('events').EventEmitter; // or require('eventmitter3');
var assert = require('assert');

var events = new EventEmitter();
var ultron = new Ultron(events);

var counter = 0;

ultron.on('event1', function(a, b, c) { assert.equal(a, 'a'); assert.equal(b, 'b'); assert.equal(c, 'c'); console.log("called event1"); counter = counter + 1}, { custom: 'function context' });
ultron.once('event2', function(a, b, c) { assert.equal(a, 'a'); assert.equal(b, 'b'); assert.equal(c, 'c'); console.log("called event2"); counter = counter + 1}, { custom: 'function context' });

counter = 0;
events.emit('event1', 'a', 'b', 'c');
events.emit('event1', 'a', 'b', 'c');
events.emit('event1', 'a', 'b', 'c');
assert.equal(counter, 3);

counter = 0;
events.emit('event2', 'a', 'b', 'c');
events.emit('event2', 'a', 'b', 'c');
events.emit('event2', 'a', 'b', 'c');
assert.equal(counter, 1);
